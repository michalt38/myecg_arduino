#ifndef _MYSTRING_H_
#define	_MYSTRING_H_
#include "command_decoder.h"

enum CompResult {DIFFERENT, EQUAL};

void CopyString(char [], char []);
enum CompResult eCompareString(char [], char []);
void AppendString(char [], char []);
void ReplaceCharactersInString(char [], char, char);
void UIntToHexStr(unsigned int, char []);
void AppendUIntToString(unsigned int, char []);
enum Result eHexStringToUInt(char [], unsigned int *);

#endif
