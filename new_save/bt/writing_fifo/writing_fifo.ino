int inbyte = 0;
char tekst[] = "HELLO";
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("Trying to open...");
  FILE *file = fopen("/tmp/ble_pipe_in", "w");
  Serial.println("Opened");
  int bytes = fwrite(tekst, sizeof(char), strlen(tekst), file);
  Serial.println("Written: " + String(bytes) + " chars");
  fclose(file);
  Serial.println("File closed");
}

void loop() {
  // put your main code here, to run repeatedly:
}
