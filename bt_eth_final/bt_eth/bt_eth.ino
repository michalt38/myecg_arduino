#include <SPI.h>
#include <SD.h>
#include <Ethernet.h>
#include <Wire.h>
#include "mystring.h"
#include "command_decoder.h"
bool bt_connection;
char received_buffer[1024];
bool save_on_sd;
bool save_on_sd_loc;
char file_name[128];
char user_name[128];
char user_id[128];
int redecode_msg;
File root;
bool isEnd;
EthernetClient client;
EthernetServer server(8089);
EthernetClient client2;
char plik[128];
int upload_status;
bool first_ajax_call;
bool upload_stop;

String readstr;
bool is_logged;
bool log_failed;
bool upload_done;
bool upload_failed;
bool is_live;
String fname;
String file_to_upload;
char get_name[2][128];
char get_value[2][128];
char post_name[2][128];
char post_value[2][128];
String data;
int sig_num;
bool is_eth_on;

char *dtostrf (float val, signed char width, unsigned char prec, char *sout) {
  char fmt[20];
  sprintf(fmt, "%%%d.%df", width, prec);
  sprintf(sout, fmt, val);
  return sout;
}

void setup() {
  Serial.begin(9600);
  system("rfkill unblock bluetooth");
  system("rm /tmp/ble_pipe_out");
  bt_connection = true;
  redecode_msg = 0;
  isEnd = true;
  Ethernet.begin(NULL);
  server.begin();
  save_on_sd = false;
  is_logged = false;
  log_failed = false;
  upload_done = false;
  upload_failed = false;
  is_live = false;
  is_eth_on = false;
  save_on_sd_loc = false;
  upload_stop = false;

  pinMode(10, INPUT); // Setup for leads off detection LO +
  pinMode(11, INPUT); // Setup for leads off detection LO -

  Serial.println("Initializing SD card... ");

  if (!SD.begin(4)) {
    Serial.println("Initialization failed!");
    return;
  }
  Serial.println("Initialization done.");


  pthread_t newThread;
  int p = pthread_create(&newThread, NULL, check_bt, (void*)0);
  if (p)
    Serial.println("Error: Unable to create thread");
  else
    pthread_detach(newThread);

}

void loop() {
  client = server.available();
  //if(!bt_connection) {
  if (client) {
    String url = process_query(client);
    if (url == "/") {
      if (is_live) is_live = false;
      send_index(client);
    }
    else if (url == "/send_live_ecg")
      send_live_ecg(client);
    else if (url == "/log_out")
      log_out(client);
    else if (url == "/log_in")
      log_in(post_value[0], post_value[1], client, client2);
    else if (url == "/upload")
      send_upload(client);
    else if (url == "/upload_signal")
      upload_signal(client);
    else if (url == "/show_signal")
      send_show_signal(client);
    else if (url == "/start_recording")
      start_recording(client);
    else if (url == "/stop_recording")
      stop_recording(client);
    else if (url.indexOf("ajax_switch") > -1)
    {
      ;
    }
    else
      send_error(client);
    client.stop();
  }
  //}
}

bool get_values(File dir) {
  Serial.println("START");
  //char values[500];
  String values = "";
  String val;
  bool isEnd = false;
  if (!dir.available()) {
    dir.close();
    char tekst[] = "GET_VALUES ERR NO_MORE_VALUES\n";
    FILE* file = fopen("/tmp/ble_pipe_in", "w");
    size_t ch_count = fwrite(tekst, sizeof(char), strlen(tekst), file);
    fclose(file);
    return false;
  }

  for (int i = 0; i < 100; i++) {
    int temp;
    val = "";
    while (true) {
      if (!dir.available())
      {
        isEnd = true;
        break;
      }
      temp = int(dir.read());
      if (!isdigit(temp)) {
        if (char(temp) == 'E') {
          val += "0";
          while (true) {
            char c = (char)dir.read();
            if (c == '\r') break;
          }
          break;
        }
        else if (char(temp) == ' ') {
          val += '_';
          continue;
        }
        else
          break;
      }
      val += char(temp);
    }
    Serial.println(val);
    values += val + ",";
    if (isEnd)
    {
      break;
    }
    if (!dir.available())
    {
      isEnd = true;
      break;
    }
    dir.read();
    if (!dir.available())
    {
      isEnd = true;
      break;
    }
  }
  char tekst[25000];
  strncpy(tekst, "GET_VALUES OK ", sizeof(tekst));
  char __values[values.length()];
  values.toCharArray(__values, sizeof(__values));
  strcat(tekst, __values);
  strcat(tekst, "\n\0");
  Serial.println(tekst);
  FILE* file = fopen("/tmp/ble_pipe_in", "w");
  size_t ch_count = fwrite(tekst, sizeof(char), strlen(tekst), file);
  fclose(file);
  //dir.close();
  return true;
}

bool getNextFile(File dir, int numTabs) {
  while (true) {

    File entry =  dir.openNextFile();
    if (!entry) {
      // no more files
      char tekst[] = "GET_FILE ERR NO_MORE_FILES\n";
      FILE* file = fopen("/tmp/ble_pipe_in", "w");
      size_t ch_count = fwrite(tekst, sizeof(char), strlen(tekst), file);
      fclose(file);
      return false;
    }
    for (uint8_t i = 0; i < numTabs; i++) {
      Serial.print('\t');
    }
    //Serial.print(entry.name());
    if (entry.isDirectory()) {
      entry.close();
      continue;
    } else {
      int i;
      char id[128];
      for (i = 0; isdigit(entry.name()[i]) && entry.name()[i] != NULL; i++)
      {
        id[i] = entry.name()[i];
      }
      id[i] = '\0';
      if (EQUAL != eCompareString(id, user_id)) {
        entry.close();
        continue;
      }
      if (save_on_sd) {
        char file[128];
        strncpy(file, entry.name(), sizeof(file));
        Serial.println(file);
        Serial.println(file_name);
        if (EQUAL == eCompareString(file, file_name)) {
          entry.close();
          continue;
        }
      }
      char tekst[64];
      strncpy(tekst, "GET_FILE OK ", sizeof(tekst));
      strcat(tekst, entry.name());
      strcat(tekst, "\n\0");
      FILE* file = fopen("/tmp/ble_pipe_in", "w");
      size_t ch_count = fwrite(tekst, sizeof(char), strlen(tekst), file);
      fclose(file);
    }
    entry.close();
    break;
  }
  return true;
}

void *save_data(void* x) {
  Serial.print("Creating file ");
  Serial.println(file_name);
  Serial.println(user_name);
  File file = SD.open(file_name, FILE_WRITE);
  bool firstIteration = true;
  long start;
  long check_time;
  if (file) {
    file.println(user_name);
    while (save_on_sd) {
      //if(!firstIteration) save_time = micros(); else save_time = 0;
      //save_time = micros();
      if ((digitalRead(10) == 1) || (digitalRead(11) == 1))
      {
        if ((digitalRead(10) == 1) && (digitalRead(11) == 1)) {
          //file.println("ERR LO-&LO+");
          if (firstIteration) {
            file.print(0);
            firstIteration = false;
          } else file.print(micros() - start);
          file.print(" ");
          file.println("ERR_LO-&LO+");
        }
        else if (digitalRead(10) == 1) {
          //file.println("ERR LO+");
          if (firstIteration) {
            file.print(0);
            firstIteration = false;
          } else file.print(micros() - start);
          file.print(" ");
          file.println("ERR_LO+");
        }
        else if (digitalRead(11) == 1) {
          //file.println("ERR LO-");
          if (firstIteration) {
            file.print(0);
            firstIteration = false;
          } else file.print(micros() - start);
          file.print(" ");
          file.println("ERR_LO-");
        }
      }
      else {
        if (firstIteration) {
          file.print(0);
          firstIteration = false;
        } else file.print(micros() - start);
        file.print(" ");
        file.println(analogRead(A0));
      }
      start = micros();
      while (micros() - start < 5000) {}
    }
  }
  else
    Serial.println("Error creating file...");
  Serial.println("Closing file...");
  file.close();
}

void send_error(EthernetClient& client)
{
  client.println("HTTP/1.1 404 Not Found");
  client.println();
}

String process_query(EthernetClient& client)
{
  String get_url;
  String line;
  bool stop_reading = true;
  bool post = false;
  int len;
  while (client.connected()) {
    //Serial.println(client.available());
    if (client.available()) {
      char c = client.read();
      //Serial.write(c);
      line += c;
      //Serial.println(line);

      if (line == "\r\n") {
        if (stop_reading)
          return get_url;
        else {
          stop_reading = true;
          line = "";
        }
      }
      else if (c == '\n') {
        if (line.indexOf("ajax_switch") > -1) {
          // read switch state and analog input
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: keep-alive");
          client.println();
          GetAjaxData(client);
        }
        if (line.startsWith("GET ")) {
          //Serial.println("GET");
          get_url = line.substring(4);
          //          int pos = get_url.indexOf(' ');
          //          if (pos != -1) {
          //            get_url = get_url.substring(0, pos);
          //          }
          int pos = get_url.indexOf('?');
          if (pos != -1) {
            int posSpace = get_url.indexOf(' ');
            String get_params = get_url.substring(pos + 1, posSpace);
            get_url = get_url.substring(0, pos);
            char curr = '\0';
            int line_idx = 0;
            int val_idx = 0;
            int row = 0;
            bool is_name = true;
            while (line_idx < get_params.length()) {
              curr = get_params.charAt(line_idx++);
              //Serial.println(curr);
              if (curr == '=') {
                get_name[row][val_idx] = '\0';
                val_idx = 0;
                is_name = false;
                continue;
              }
              if (curr == '&') {
                get_value[row][val_idx] = '\0';
                val_idx = 0;
                row++;
                is_name = true;
                continue;
              }
              if (is_name) get_name[row][val_idx++] = curr;
              else get_value[row][val_idx++] = curr;
            }

          } else {
            int pos = get_url.indexOf(' ');
            if (pos != -1) {
              get_url = get_url.substring(0, pos);
            }
          }

          get_url.trim();


        }
        else if (line.startsWith("POST ")) {
          //Serial.println("POST");
          stop_reading = false;
          post = true;
          get_url = line.substring(5);
          int pos = get_url.indexOf(' ');
          if (pos != -1) {
            get_url = get_url.substring(0, pos);
          }
          get_url.trim();
        } else if (post && line.startsWith("Content-Length: ")) {
          String content_len;
          content_len = line.substring(16);
          int pos = content_len.indexOf(' ');
          if (pos != -1) {
            content_len = content_len.substring(0, pos);
          }
          len = content_len.toInt();
          //          Serial.print("Length: ");
          //          Serial.println(len);
        }
        line = "";
      }
    } else if (stop_reading && post) {
      //Serial.println("STOP POST");
      char curr = '\0';
      int line_idx = 0;
      int val_idx = 0;
      int row = 0;
      bool is_name = true;
      while (line_idx < len) {
        curr = line.charAt(line_idx++);
        //Serial.println(curr);
        if (curr == '=') {
          post_name[row][val_idx] = '\0';
          val_idx = 0;
          is_name = false;
          continue;
        }
        if (curr == '&') {
          post_value[row][val_idx] = '\0';
          val_idx = 0;
          row++;
          is_name = true;
          continue;
        }
        if (is_name) post_name[row][val_idx++] = curr;
        else post_value[row][val_idx++] = curr;
      }
      //          Serial.println();
      //          Serial.print(post_name[0]);
      //          Serial.print(" ");
      //          Serial.print(post_value[0]);
      //          Serial.print(" ");
      //          Serial.print(post_name[1]);
      //          Serial.print(" ");
      //          Serial.print(post_value[1]);
      Serial.println(get_url == "/");
      Serial.println(String("email") == String(post_name[0]));
      //if (get_url == "/" && String("email") == String(post_name[0])) log_in(post_value[0], post_value[1], client, client2);
      return get_url;
    }
  }
}

void *loc_save_data(void* x) {
  Serial.print("Creating file ");
  Serial.println(fname);
  Serial.println(user_name);
  fname.toCharArray(file_name, 128);
  File file = SD.open(file_name, FILE_WRITE);
  bool firstIteration = true;
  long start;
  long check_time;
  if (file) {
    file.println(user_name);
    while (save_on_sd_loc) {
      //if(!firstIteration) save_time = micros(); else save_time = 0;
      //save_time = micros();
      if ((digitalRead(10) == 1) || (digitalRead(11) == 1))
      {
        if ((digitalRead(10) == 1) && (digitalRead(11) == 1)) {
          //file.println("ERR LO-&LO+");
          if (firstIteration) {
            file.print(0);
            firstIteration = false;
          } else file.print(micros() - start);
          file.print(" ");
          file.println("ERR_LO-&LO+");
        }
        else if (digitalRead(10) == 1) {
          //file.println("ERR LO+");
          if (firstIteration) {
            file.print(0);
            firstIteration = false;
          } else file.print(micros() - start);
          file.print(" ");
          file.println("ERR_LO+");
        }
        else if (digitalRead(11) == 1) {
          //file.println("ERR LO-");
          if (firstIteration) {
            file.print(0);
            firstIteration = false;
          } else file.print(micros() - start);
          file.print(" ");
          file.println("ERR_LO-");
        }
      }
      else {
        if (firstIteration) {
          file.print(0);
          firstIteration = false;
        } else file.print(micros() - start);
        file.print(" ");
        file.println(analogRead(A0));
      }
      start = micros();
      while (micros() - start < 5000) {}
    }
  }
  else
    Serial.println("Error creating file...");
  Serial.println("Closing file...");
  file.close();
}

void send_index(EthernetClient& client) {

  //if(is_uploading) send_index_upload(client);

  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/html");
  client.println("Connection: close");
  client.println();
  client.println("<!DOCTYPE HTML>");
  client.println("<html>");
  client.println("<head>");
  client.println("<meta charset=\"utf-8\" />");
  client.println("<title>myECG</title>");
  client.println("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">");
  client.println("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css\">");
  client.println("</head>");
  client.println("<body>");
  client.println("<div class=\"container\">");
  client.println("<center><h1>myECG</h1></center>");
  if (save_on_sd_loc)
    client.println("<br><center>Signal is being recorded to: " + fname + "</center>");
  if (upload_failed)
    client.println("<br><center>Uploading file " + file_to_upload + " failed</center>");
  if (upload_done)
    client.println("<br><center>File " + file_to_upload + " uploaded successfully!</center>");
  if (log_failed) {
    client.println("<br>");
    client.println("<div class=\"col-sm-offset-3 col-sm-6\"> <div class=\"alert alert-danger\"> Invalid e-mail or password. </div> </div>");
    log_failed = false;
  }
  if (!is_logged) {
    client.println("<br>");
    client.println("  <form action=\"/log_in\" method=\"POST\">  <div class=\"container\">   <div class=\"row\">     <div class=\"col-sm-offset-3 col-sm-6\">      <div class=\"input-group\">         <span class=\"input-group-addon\"><i class=\"fa fa-user\"></i></span>         <input id=\"email\" type=\"text\" class=\"form-control\" name=\"email\" placeholder=\"E-mail\">       </div>    <br>      <div class=\"input-group\">         <span class=\"input-group-addon\"><i class=\"fa fa-lock\"></i></span>         <input id=\"password\" type=\"password\" class=\"form-control\" name=\"password\" placeholder=\"Password\">       </div>  <br>  <center>    <input type=\"submit\" class=\"btn btn-info center-block\" value=\"Log In\">  </center>   </div>  </div>  </div>  </form>");
  } else {
    client.println("<br>");
    client.println("<center>");
    client.println("<div class=\"panel panel-default\" style=\"max-width: 180px\">");
    client.println("<div class=\"panel-body\">");
    client.println("<center>");
    if (save_on_sd_loc)
      client.println("<a href=\"/stop_recording\" class=\"btn btn-danger\" role=\"button\" style=\"width:140px;height:32px\">Stop Recording</a>");
    else
      client.println("<a href=\"/start_recording\" class=\"btn btn-danger\" role=\"button\" style=\"width:140px;height:32px\">Start Recording</a>");
    client.println("<br><br>");
    client.println("<a href=\"/send_live_ecg\" class=\"btn btn-default\" role=\"button\" style=\"width:140px;height:32px\">Live ECG</a>");
    client.println("<br><br>");
    client.println("<a href=\"/upload\" class=\"btn btn-default\" role=\"button\" style=\"width:140px;height:32px\">Upload</a>");
    client.println("<br><br>");
    client.println("<a href=\"/log_out\" class=\"btn btn-default\" role=\"button\" style=\"width:140px;height:32px\">Log out</a>");
  }
  client.println("</center>");
  client.println("</div>");
  client.println("</div>");
  client.println("</center>");
  client.println("</div>");
  client.println("</body>");
  client.println("</html>");
  client.stop();

  if (upload_done) upload_done = false; if (upload_failed) upload_failed = false;
}

String getNextFileStr(File dir, int numTabs) {
  char tekst[64];
  while (true) {

    File entry =  dir.openNextFile();
    if (!entry) {
      // no more files
      char tekst[] = "GET_FILE ERR NO_MORE_FILES\n";
      FILE* file = fopen("/tmp/ble_pipe_in", "w");
      size_t ch_count = fwrite(tekst, sizeof(char), strlen(tekst), file);
      fclose(file);
      return String("");
    }
    for (uint8_t i = 0; i < numTabs; i++) {
      Serial.print('\t');
    }
    //Serial.print(entry.name());
    if (entry.isDirectory()) {
      entry.close();
      continue;
    } else {
      int i;
      char id[128];
      for (i = 0; isdigit(entry.name()[i]) && entry.name()[i] != NULL; i++)
      {
        id[i] = entry.name()[i];
      }
      id[i] = '\0';
      if (EQUAL != eCompareString(id, user_id)) {
        entry.close();
        continue;
      }
      if (save_on_sd_loc) {
        char file[128];
        strncpy(file, entry.name(), sizeof(file));
        Serial.println(file);
        Serial.println(file_name);
        if (EQUAL == eCompareString(file, file_name)) {
          entry.close();
          continue;
        }
      }
      strncpy(tekst, entry.name(), sizeof(tekst));
      strcat(tekst, "\0");
    }
    entry.close();
    break;
  }
  return String(tekst);
}

void log_out(EthernetClient& client) {
  is_eth_on = false;
  is_logged = false;
  send_index(client);
}

void send_upload(EthernetClient& client) {
  if (!is_logged) {
    send_index(client);
    return;
  }
  String sig;
  File dir = SD.open("/");

  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/html");
  client.println("Connection: close");
  client.println();
  client.println("<!DOCTYPE HTML>");
  client.println("<html>");
  client.println("<head>");
  client.println("<meta charset=\"utf-8\" />");
  client.println("<title>myECG</title>");
  client.println("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js\"></script>");
  client.println("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">");
  client.println("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css\">");
  client.println("<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script>");
  client.println("</head>");
  client.println("<body>");
  client.println("<div class=\"container\">");
  //client.println("<div style=\"display: inline\"><a href=\"/\" class=\"btn btn-default pull-left\" role=\"button\" style=\"width:140px;height:32px\">Back</a></div><div style=\"display: inline\"><center><h1>myECG</h1></center></div><br>");
  client.println("<div class=\"row\"> <div class=\"col-sm-4\"> <a href=\"/\" class=\"btn btn-default pull-left\" role=\"button\"><i class=\"fa fa-arrow-left\" aria-hidden=\"true\"></i></a> </div> <div class=\"col-sm-4\"> <center><h1>myECG</h1></center> </div> <div class=\"col-sm-4\"> </div> </div><br>");
  client.println("<table class=\"table table-hover\">");
  while (true) {
    sig = getNextFileStr(dir, 0); if (sig == "") break;
    if (sig != "") {
      client.println("<tr><td><a href=\"upload_signal?file=" + sig + "\" onclick=\"showModal()\">" + sig + "</a></tr></td>");
    }
  }
  client.println("</table>");
  client.println("</tbody>");
  client.println("</div>");
  client.println("<div class=\"modal fade\" id=\"myModal\" role=\"dialog\" data-keyboard=\"false\" data-backdrop=\"static\"><div class=\"modal-dailog\"><div class=\"modal-content\"><div class=\"modal-body\"><center><p>File is being uploaded. Please wait...</p></center></div></div></div></div>");
  client.println("<script>function showModal(){$(\"#myModal\").modal();}</script>");
  client.println("</body>");
  client.println("</html>");
  client.stop();
}

void send_show_signal(EthernetClient& client) {
  if (!is_logged) {
    send_index(client);
    return;
  }

  File file_dir = SD.open(get_value[0], FILE_READ);
  while (file_dir.available()) {
    char c = (char)file_dir.read();
    if (c == '\n') break;
  }

  String values = "";
  String val = "";
  bool isEnd = false;
  float probe_time = 0;
  int ival = 0;
  char val_buff[13];

  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/html");
  client.println("Connection: close");
  client.println();
  client.println("<!DOCTYPE HTML>");
  client.println("<html>");
  client.println("<head>");
  client.println("<meta charset=\"utf-8\" />");
  client.println("<title>myECG</title>");
  client.println("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">");
  client.println("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css\">");
  client.println("<script src=\"https://canvasjs.com/assets/script/canvasjs.min.js\"></script>");
  client.println("<script> window.onload = function () {var chart = new CanvasJS.Chart(\"chartContainer\", { animationEnabled: true, zoomEnabled: true, theme: \"light2\", axisX :{title: \"Time [s]\"}, axisY:{title: \"Value\", includeZero: false }, data: [{ type: \"line\", dataPoints: [");

  for (int i = 0; ; i++) {
    int temp;
    values = "{x: ";
    val = "";
    while (true) {
      if (!file_dir.available())
      {
        isEnd = true;
        break;
      }
      temp = int(file_dir.read());
      if (!isdigit(temp)) {
        if (char(temp) == 'E') {
          val += "0";
          while (true) {
            char c = (char)file_dir.read();
            if (c == '\r') break;
          }
          break;
        }
        else if (char(temp) == ' ') {
          ival = val.toInt();
          probe_time += float(ival) / 1000000;
          dtostrf(probe_time, 7, 6, val_buff);
          values += String(val_buff) + ", y: ";
          val = "";
          continue;
        }
        else
          break;
      }
      val += char(temp);
    }
    //ival = val.toInt();
    //probe_time += ival/1000000;
    //dtostrf(probe_time, 7, 6, val_buff);
    values += val + "},";
    //Serial.println(values);
    client.println(values);
    if (isEnd)
    {
      break;
    }
    if (!file_dir.available())
    {
      isEnd = true;
      break;
    }
    file_dir.read();
    if (!file_dir.available())
    {
      isEnd = true;
      break;
    }
  }

  client.println("] }] }); chart.render(); } </script>");
  client.println("</head>");
  client.println("<body>");
  client.println("<div class=\"container\">");
  client.println("<a href=\"/upload\" class=\"btn btn-default pull-left\" role=\"button\" style=\"width:140px;height:32px\">Back</a><center><h1>myECG</h1></center><br>");
  client.println("<div id=\"chartContainer\" style=\"height: 370px; width: 100%;\"></div>");


  client.println("</div>");
  client.println("</body>");
  client.println("</html>");
  client.stop();
}

void upload_signal(EthernetClient& client) {

  file_to_upload = String(get_value[0]);
  upload_file();
  //  pthread_t newThread;
  //  int p = pthread_create(&newThread, NULL, upload_file, (void*)0);
  //  if(p)
  //    Serial.println("Error: Unable to create thread");
  //  else
  //    pthread_detach(newThread);

}

void upload_file() {
  char plik[30];
  file_to_upload.toCharArray(plik, 30);
  String date = file_to_upload;
  File myFile = SD.open(plik, FILE_READ);
  char sig_data[45];
  String sdata = "";
  //strncpy(datetime, "date=", sizeof(datetime));
  //strcat(datetime, plik);
  //  datetime[15] = ' ';
  //  datetime[18] = ':';
  //  datetime[21] = ':';
  //  char stime[10];
  //  for(int i = 0; datetime[i] != NULL; i++)
  //    stime[i] = datetime[i+16];
  //  datetime[15] = '%';
  //  datetime[16] = '2';
  //  datetime[17] = '0';
  //  datetime[18] = '\0';
  //  strcat(datetime, stime);
  //  Serial.println(datetime);
  int pos = date.indexOf('.');
  date = date.substring(2, pos);
  //date.setCharAt(10, ' ');
  date.setCharAt(13, ':');
  date.setCharAt(16, ':');
  date = "date=" + date;
  sdata = "user=" + String(user_id) + "&" + date;
  sdata.toCharArray(sig_data, 45);

  send_data(client2, "/~milchalt/myECG/add_signal.php?", sig_data);

  //int values[] = {1,2,3,4,5};
  //char data[1500];
  //char val[3];
  bool isEnd = false;
  if (!myFile.available()) {
    myFile.close();
    upload_failed = true;
    send_index(client);
    return;
  }
  while (myFile.available()) {
    char c = (char)myFile.read();
    if (c == '\n') break;
  }
  String data = "";
  while (!bt_connection) {
    data = "";
    for (int i = 0; i < 300; i++) {
      int temp;
      if (i == 0) data += "time[]="; else data += "&time[]=";
      while (true) {
        if (!myFile.available())
        {
          isEnd = true;
          break;
        }
        temp = int(myFile.read());
        if (!isdigit(temp)) {
          if (char(temp) == 'E') {
            data += "0";
            while (true) {
              char c = (char)myFile.read();
              if (c == '\r') break;
            }
            break;
          }
          else if (char(temp) == ' ') {
            data += "&value[]=";
            continue;
          }
          else
            break;
        }
        data += char(temp);
      }
      if (isEnd)
      {
        break;
      }
      if (!myFile.available())
      {
        isEnd = true;
        break;
      }
      myFile.read();
      if (!myFile.available())
      {
        isEnd = true;
        break;
      }
      //Serial.println(i);
    }
    //Serial.println(data);
    char data_buff[12500];
    data.toCharArray(data_buff, 12500);
    send_data(client2, "/~milchalt/myECG/add.php?", data_buff);
    if (isEnd)
      break;
  }
  //Serial.println("HERE");
  if (myFile)
    myFile.close();

  upload_done = true;
  send_index(client);
}

void send_data(EthernetClient& client_database, char* address, char* data)
{
  //char datac[16];
  //memset(datac,0,16);
  //sprintf(datac, "%.2f", bme.getTemperature_C());
  //data = "temp=" + String(datac);

  bool udalo_sie = true;
  if (client_database.connect("student.agh.edu.pl", 80))
  {
    //Serial.println("POST " + String(address) + String(data) +" HTTP/1.1");
    client_database.println("POST " + String(address) + String(data) + " HTTP/1.1");
    Serial.println(String(address) + String(data));
    //client_database.println("POST /~milchalt/test/add.php?"+data+" HTTP/1.1");
    //client_database.println("GET /~milchalt/test/add.php? HTTP/1.1");
    client_database.println("Host: student.agh.edu.pl"); // SERVER ADDRESS HERE TOO

    client_database.println("Content-Type: application/x-www-form-urlencoded");
    client_database.print("Content-Length: ");
    client_database.println(String(data).length());
    client_database.println();
    client_database.print(data);

  }
  else
    udalo_sie = false;

  if (client_database.connected()) {
    client_database.stop(); // DISCONNECT FROM THE SERVER
  }
  Serial.println("DONE " + String(udalo_sie));

}

void log_in(String email, String password, EthernetClient& client, EthernetClient& client_database) {
  String data = "email=" + email + "&password=" + password;
  if (client_database.connect("student.agh.edu.pl", 80))
  {
    client_database.println("POST /~milchalt/myECG/login.php?" + data + " HTTP/1.1");
    client_database.println("Host: student.agh.edu.pl"); // SERVER ADDRESS HERE TOO
    client_database.println("Content-Type: application/x-www-form-urlencoded");
    client_database.print("Content-Length: ");
    client_database.println(data.length());
    client_database.println();
    client_database.print(data);

  } else Serial.println("CONNECTION ERROR");

  String line = "";
  String name_surname = "";
  while (client_database.connected())
  {
    if (client_database.available()) {
      char c = client_database.read();
      Serial.print(c);
      line += c;

      if (c == '\n') {
        if (line.startsWith("Login ")) {
          String log_status;
          log_status = line.substring(6);
          int pos = log_status.indexOf('\n');
          if (pos != -1) {
            log_status = log_status.substring(0, pos);
          }
          Serial.println(log_status);
          //char log_cmp[24];
          //log_status.toCharArray(log_cmp, 24);
          Serial.print("Is failed ");
          Serial.println(log_status == String("failed"));
          if (log_status == "success") {
            is_logged = true;
          }
          else if (log_status == String("failed"))
            log_failed = true;
        }
        if (line.startsWith("id")) {
          String id;
          id = line.substring(3);
          int pos = id.indexOf('\n');
          if (pos != -1) {
            id = id.substring(0, pos);
          }
          id.toCharArray(user_id, 128);
        }
        if (line.startsWith("name")) {
          String uname;
          uname = line.substring(5);
          int pos = uname.indexOf('\n');
          if (pos != -1) {
            uname = uname.substring(0, pos);
          }
          name_surname = uname + "_";
        }
        if (line.startsWith("surname")) {
          String surname;
          surname = line.substring(8);
          int pos = surname.indexOf('\n');
          if (pos != -1) {
            surname = surname.substring(0, pos);
          }
          name_surname += surname;
          name_surname.toCharArray(user_name, 128);
        }
        line = "";
      }
    } else
      client_database.stop();
  }
  is_eth_on = true;
  send_index(client);
}

void start_recording(EthernetClient& client)
{
  save_on_sd_loc = true;
  //CopyString("TEST.txt", file_name);
  time_t t = time(NULL);
  struct tm tm = *localtime(&t);
  char buf[100];

  int h = tm.tm_hour;
  if (tm.tm_hour >= 24) {
    while (h >= 24)
      h -= 24;
  }
  sprintf(buf, "_%d-%02d-%02d_%02d-%02d-%02d\0", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, h + 2, tm.tm_min, tm.tm_sec);
  //Serial.println(buf);

  strncpy(file_name, user_id, 128);
  strcat(file_name, buf);
  strcat(file_name, ".txt\0");
  Serial.println(file_name);
  fname = String(file_name);
  pthread_t newThread;
  int p = pthread_create(&newThread, NULL, loc_save_data, (void*)0);
  if (p)
    Serial.println("Error: Unable to create thread");
  else
    pthread_detach(newThread);

  send_index(client);
}

void stop_recording(EthernetClient& client)
{
  save_on_sd_loc = false;
  send_index(client);
}

void GetAjaxData(EthernetClient cl)
{
  if (!is_live) return;
  int analog_val;

  int curr;
  int static mtime;
  if(first_ajax_call) {curr = 0; first_ajax_call = false;}
  else curr = micros() - mtime;
  mtime = micros();
  // read analog pin A0
  analog_val = analogRead(0);
  cl.print("Time: ");
  cl.print(curr);
  cl.print(" Value: ");
  cl.print(analog_val);
  cl.println("");
}

void send_live_ecg(EthernetClient& client) {
  is_live = true;
  first_ajax_call = true;
  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/html");
  client.println("Connection: keep-alive");
  client.println();
  client.println("<!DOCTYPE html>");
  client.println("<html>");
  client.println("<head>");
  client.println("<title>myECG</title>");
  client.println("<script src=\"https://canvasjs.com/assets/script/canvasjs.min.js\"></script>");
  client.println("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">");
  client.println("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css\">");
  client.println("<script>");
  client.println("var dps = []; var chart; var vx = 0.0; var size_limit = 1000;");
  client.println("function GetSwitchAnalogData() {");
  client.println("nocache = \"&nocache=\" + Math.random() * 1000000;");
  client.println("var request = new XMLHttpRequest();");
  client.println("request.onreadystatechange = function() {");
  client.println("if (this.readyState == 4) {");
  client.println("if (this.status == 200) {");
  client.println("if (this.responseText != null) {");
  //client.println("document.getElementById(\"sw_an_data\").innerHTML = this.responseText;");
  client.println("var str = this.responseText; var t = str.substring(6); var pos = t.indexOf(\" \"); t = t.substring(0, pos); pos += 6; var v = str.substring(pos); pos = v.indexOf(\":\"); var v = v.substring(pos+2);");
  client.println("var ti = parseFloat(t)/1000000; vx += ti; dps.push({x: vx, y: parseInt(v)}); if(dps.length > size_limit) { dps.shift();} chart.render();");
  //client.println("dps.push({x: vx, y: parseInt(v)}); vx++; if(dps.length > size_limit) { dps.shift();} chart.render();");
  client.println("}}}}");
  client.println("request.open(\"GET\", \"ajax_switch\" + nocache, true);");
  client.println("request.send(null);");
  client.println("setTimeout('GetSwitchAnalogData()', 7);");
  client.println("}");
  //client.println("window.onload = function () {console.log(\"HERE\");chart = new CanvasJS.Chart(\"chartContainer\", { axisY: { includeZero: false }, data: [{ type: \"line\", dataPoints: dps }] });}");
  client.println("function onLoad() {chart = new CanvasJS.Chart(\"chartContainer\", { axisX : {title: \"Time [s]\"}, axisY: {title: \"ADC Value\", includeZero: false }, data: [{ type: \"line\", dataPoints: dps }] }); GetSwitchAnalogData();}");
  client.println("function SetProbesNum() {var size_limit = document.getElementById(\"number\").value;}");
  client.println("</script>");
  client.println("</head>");
  client.println("<body onload=\"onLoad()\">");
  client.println("<div class=\"container\">");
  client.println("<div class=\"row\"> <div class=\"col-sm-4\"> <a href=\"/\" class=\"btn btn-default pull-left\" role=\"button\"><i class=\"fa fa-arrow-left\" aria-hidden=\"true\"></i></a> </div> <div class=\"col-sm-4\"> <center><h1>myECG</h1></center> </div> <div class=\"col-sm-4\"> </div> </div><br>");
  //client.println("<div id=\"sw_an_data\">");
  client.println("<div id=\"chartContainer\" style=\"height: 370px; width:100%;\"></div><br>");
  client.println("</div>");
  client.println("</body>");
  client.println("</html>");
}

void* check_bt(void *x) {
  while (true) {
    //if(is_eth_on) continue;
    uint8_t value;
    FILE* f = fopen("/tmp/ble_pipe_out", "rb");
    //FILE* f = fopen("/dev/rfcomm0", "rb");
    if (!f) {
      if (bt_connection) {
        Serial.println("Bluetooth not connected");
        system("python /home/root/my-SPP-pipe-out.py &");
        //system("rfcomm listen 0 1 &");
        Serial.println("Listening...");
        bt_connection = false;
      }
    }
    else {
      if (bt_connection == false) {
        Serial.println("Bluetooth connected");
        bt_connection = true;
      }

      if (fgets(received_buffer, 1024, f) == NULL) {
        Serial.println("Unable to read value");
        system("rm /tmp/ble_pipe_out");
        system("rm /tmp/ble_pipe_in");
      }
      else {
REDECODE_MSG:
        for (int i = 0; received_buffer[i] != NULL; i++) {
          //Serial.print(received_buffer[i]);
          Receiver_PutCharacterToBuffer(received_buffer[i]);
        }
        //Serial.println();
        /*
          if(fread(&value,1,1,f)!=1) {
           Serial.println("Unable to read value");
           system("rm /tmp/ble_pipe_out");
           system("rm /tmp/ble_pipe_in");
          }
          else{
           Serial.print(char(value));

           Receiver_PutCharacterToBuffer(char(value));
        */
        if (READY == eReciver_GetStatus())
        {
          //Serial.println("RECEIVER READY");
          char received_string[512];
          KeywordCode eTokenKey;

          Receiver_GetStringCopy(received_string);
          //Serial.println("Received string: " + String(received_string));
          DecodeMsg(received_string);
          if (OK == eToken_GetKeywordCode(0, &eTokenKey))
          {
            switch (eTokenKey)
            {
              case HELLO:
                {
                  char tekst[] = "Hi!!!\n";
                  FILE* file = fopen("/tmp/ble_pipe_in", "w");
                  size_t ch_count = fwrite(tekst, sizeof(char), strlen(tekst), file);
                  fclose(file);
                  //Serial.println("Written: " + String((int)ch_count) + " chars");
                  //system("echo \"10\r\n\" > /dev/rfcomm0");
                  break;
                }
              case GET_ECG:
                {
                  char tekst[32];
                  if ((digitalRead(10) == 1) || (digitalRead(11) == 1))
                  {
                    if ((digitalRead(10) == 1) && (digitalRead(11) == 1))
                      strncpy(tekst, "GET_ECG ERR LO-&LO+\n\0", sizeof(tekst));
                    else if (digitalRead(10) == 1)
                      strncpy(tekst, "GET_ECG ERR LO+\n\0", sizeof(tekst));
                    else if (digitalRead(11) == 1)
                      strncpy(tekst, "GET_ECG ERR LO-\n\0", sizeof(tekst));
                  }
                  else
                  {
                    char num[4];
                    sprintf(num, "%d", analogRead(A0));
                    strncpy(tekst, "GET_ECG OK ", sizeof(tekst));
                    strcat(tekst, num);
                    strcat(tekst, "\n\0");
                  }
                  //Serial.println(tekst);
                  FILE* file = fopen("/tmp/ble_pipe_in", "w");
                  size_t ch_count = fwrite(tekst, sizeof(char), strlen(tekst), file);
                  fclose(file);
                  break;
                }
              case SAVE_ON_SD:
                {
                  if (OK == eToken_GetKeywordCode(1, &eTokenKey)) {
                    if (eTokenKey == ON) {
                      if (OK == eToken_GetString(2, file_name)) {
                        if (OK == eToken_GetString(3, user_name)) {
                          save_on_sd = true;
                          pthread_t newThread;
                          int p = pthread_create(&newThread, NULL, save_data, (void*)0);
                          if (p)
                            Serial.println("Error: Unable to create thread");
                          else
                            pthread_detach(newThread);
                        }
                      }
                    } else {
                      save_on_sd = false;
                    }
                    char tekst[] = "SAVE_ON_SD OK\n";
                    FILE* file = fopen("/tmp/ble_pipe_in", "w");
                    size_t ch_count = fwrite(tekst, sizeof(char), strlen(tekst), file);
                    fclose(file);
                  } else {
                    char tekst[] = "SAVE_ON_SD ERR\n";
                    FILE* file = fopen("/tmp/ble_pipe_in", "w");
                    size_t ch_count = fwrite(tekst, sizeof(char), strlen(tekst), file);
                    fclose(file);
                  }
                  break;
                }
              case UPLOAD_STOP: 
              {
                upload_stop = true;
                char tekst[] = "UPLOAD_STOP OK\n";
                FILE* file = fopen("/tmp/ble_pipe_in", "w");
                size_t ch_count = fwrite(tekst, sizeof(char), strlen(tekst), file);
                fclose(file);
              }
              case UNMOUNT_SD:
                {
                  system("umount /media/sdcard");
                  Serial.println("SD CARD UNMOUNTED");
                  char tekst[] = "UNMOUNT_SD OK\n";
                  FILE* file = fopen("/tmp/ble_pipe_in", "w");
                  size_t ch_count = fwrite(tekst, sizeof(char), strlen(tekst), file);
                  fclose(file);
                  break;
                }
              case GET_FILE:
                {
                  if (OK == eToken_GetString(1, user_id)) {
                    if (isEnd)
                      root = SD.open("/");
                    if (getNextFile(root, 0))
                      isEnd = false;
                    else
                      isEnd = true;

                  }
                  break;
                }
              case GET_VALUES:
                {
                  Serial.println("GET_VALUES");
                  if (OK == eToken_GetString(1, file_name)) {
                    if (isEnd) {
                      Serial.println("END");
                      root = SD.open(file_name, FILE_READ);
                      while (root.available()) {
                        char c = (char)root.read();
                        if (c == '\n') break;
                      }
                    }
                    Serial.println("FUNCTION");
                    if (get_values(root))
                      isEnd = false;
                    else
                      isEnd = true;
                  }
                  break;
                }
              case UPLOAD:
                {
                  if (OK == eToken_GetString(1, plik)) {
                    upload_status = 0;
                    save_on_sd = false;
                    /*
                      pthread_t newThread;
                      int p = pthread_create(&newThread, NULL, upload_file, (void*)0);
                      if(p)
                      Serial.println("Error: Unable to create thread");
                      else
                      pthread_detach(newThread);
                    */
                  }
                  char tekst[] = "UPLOAD OK\n";
                  FILE* file = fopen("/tmp/ble_pipe_in", "w");
                  size_t ch_count = fwrite(tekst, sizeof(char), strlen(tekst), file);
                  fclose(file);
                  break;
                }
              case GET_UPLOAD_STATUS:
                {
                  char tekst[32];
                  char num[4];
                  sprintf(num, "%d", upload_status);
                  strncpy(tekst, "GET_UPLOAD_STATUS OK ", sizeof(tekst));
                  strcat(tekst, num);
                  strcat(tekst, "\n\0");

                  FILE* file = fopen("/tmp/ble_pipe_in", "w");
                  size_t ch_count = fwrite(tekst, sizeof(char), strlen(tekst), file);
                  fclose(file);
                  break;
                }
            }
          }
          else {
            if (redecode_msg < 10)
            {
              redecode_msg++;
              goto REDECODE_MSG;
            }
            redecode_msg = 0;
            Serial.println("WRONG TOKEN");
            char tekst[] = "WRONG_TOKEN\n";
            FILE* file = fopen("/tmp/ble_pipe_in", "w");
            size_t ch_count = fwrite(tekst, sizeof(char), strlen(tekst), file);
            fclose(file);
          }
          memset(received_buffer, 0 , 1024);
        }
      }
      fclose(f);
    }
  }
}

