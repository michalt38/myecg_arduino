#ifndef __command_decoder_H
#define __command_decoder_H
#define RECEIVER_SIZE 128
#define RECEIVER_TERMINATOR 10

typedef enum KeywordCode {HELLO, GET_ECG, SAVE_ON_SD, ON, OFF, UNMOUNT_SD, GET_FILE, UPLOAD, GET_UPLOAD_STATUS, GET_VALUES, UPLOAD_STOP} KeywordCode;
enum eReceiverStatus {EMPTY, READY, _OVERFLOW};
enum Result {OK, ERR};

struct ReceiverBuffer {
  char cData[RECEIVER_SIZE];
  unsigned char ucCharCtr;
  enum eReceiverStatus eStatus;
};

unsigned char ucFindTokensInString(char *);
enum Result eStringKeyword(char [], KeywordCode *);
void DecodeTokens(void); 
void DecodeMsg(char *);
enum Result eToken_GetKeywordCode(unsigned char, KeywordCode*);
enum Result eToken_GetNumber(unsigned char, unsigned int*);
enum Result eToken_GetString(unsigned char ucIndex, char* ucDestination);

void Receiver_PutCharacterToBuffer(char cCharacter);
enum eReceiverStatus eReciver_GetStatus();
void Receiver_GetStringCopy(char * ucDestination);
#endif
