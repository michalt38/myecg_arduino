#include "command_decoder.h"
bool bt_connection;
char received_buffer[1024];

void setup() {
  Serial.begin(9600);
  system("rfkill unblock bluetooth");
  system("rm /tmp/ble_pipe_out");
  bt_connection = true;
}

void loop() {
  uint8_t value;
  FILE* f = fopen("/tmp/ble_pipe_out", "rb");
  //FILE* f = fopen("/dev/rfcomm0", "rb");
  if(!f){
    if(bt_connection){
    Serial.println("Bluetooth not connected");
    system("python /home/root/my-SPP-pipe-out.py &");
    //system("rfcomm listen 0 1 &");
    Serial.println("Listening...");
    bt_connection = false;
    }
  }
  else {
  if(bt_connection == false){
  Serial.println("Bluetooth connected");
  bt_connection = true;
  }
      
   if(fgets(received_buffer,1024,f)==NULL) {
      Serial.println("Unable to read value");
      system("rm /tmp/ble_pipe_out");
      system("rm /tmp/ble_pipe_in");
    }
    else{
      for(int i = 0; received_buffer[i] != NULL; i++) {
        Serial.print(received_buffer[i]);
        Receiver_PutCharacterToBuffer(received_buffer[i]);
      }
      //Serial.println();
   /*
    if(fread(&value,1,1,f)!=1) {
      Serial.println("Unable to read value");
      system("rm /tmp/ble_pipe_out");
      system("rm /tmp/ble_pipe_in");
    }
    else{
      Serial.print(char(value));
      
      Receiver_PutCharacterToBuffer(char(value));
      */
      if(READY == eReciver_GetStatus())
      {
        Serial.println("RECEIVER READY");
        char received_string[512];
        KeywordCode eTokenKey;

        Receiver_GetStringCopy(received_string);
        Serial.println("Received string: " + String(received_string));
        DecodeMsg(received_string);
        if(OK == eToken_GetKeywordCode(0,&eTokenKey))
        {
          switch(eTokenKey)
          {
            case HELLO:
            char tekst[] = "Hi!!!\n";
            FILE* file = fopen("/tmp/ble_pipe_in", "w");
            size_t ch_count = fwrite(tekst, sizeof(char), strlen(tekst), file);
            fclose(file);
            Serial.println("Written: " + String((int)ch_count) + " chars");
            system("echo \"10\r\n\" > /dev/rfcomm0");
            break;
          }
        }
        else
          Serial.println("WRONG TOKEN");
      }
    }
  fclose(f);
  }
}
