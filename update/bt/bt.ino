#include <SPI.h>
#include <SD.h>
#include <Ethernet.h>
#include <Wire.h>
#include "mystring.h"
#include "command_decoder.h"
bool bt_connection;
char received_buffer[1024];
bool save_on_sd;
char file_name[128];
char user_name[128];
char user_id[128];
int redecode_msg;
File root;
//char filesList[512][32];
bool isEnd;
EthernetClient client;
char plik[128];
int upload_status;

void setup() {
  Serial.begin(9600);
  system("rfkill unblock bluetooth");
  system("rm /tmp/ble_pipe_out");
  bt_connection = true;
  redecode_msg = 0;
  isEnd = true;

  pinMode(10, INPUT); // Setup for leads off detection LO +
  pinMode(11, INPUT); // Setup for leads off detection LO -

  Serial.println("Initializing SD card... ");
  
  if (!SD.begin(4)) {
    Serial.println("Initialization failed!");
    return;
  }
  Serial.println("Initialization done.");
}

void loop() {
  uint8_t value;
  FILE* f = fopen("/tmp/ble_pipe_out", "rb");
  //FILE* f = fopen("/dev/rfcomm0", "rb");
  if(!f){
    if(bt_connection){
    Serial.println("Bluetooth not connected");
    system("python /home/root/my-SPP-pipe-out.py &");
    //system("rfcomm listen 0 1 &");
    Serial.println("Listening...");
    bt_connection = false;
    }
  }
  else {
  if(bt_connection == false){
  Serial.println("Bluetooth connected");
  bt_connection = true;
  }
      
   if(fgets(received_buffer,1024,f)==NULL) {
      Serial.println("Unable to read value");
      system("rm /tmp/ble_pipe_out");
      system("rm /tmp/ble_pipe_in");
    }
    else{
      REDECODE_MSG:
      for(int i = 0; received_buffer[i] != NULL; i++) {
        //Serial.print(received_buffer[i]);
        Receiver_PutCharacterToBuffer(received_buffer[i]);
      }
      //Serial.println();
   /*
    if(fread(&value,1,1,f)!=1) {
      Serial.println("Unable to read value");
      system("rm /tmp/ble_pipe_out");
      system("rm /tmp/ble_pipe_in");
    }
    else{
      Serial.print(char(value));
      
      Receiver_PutCharacterToBuffer(char(value));
      */
      if(READY == eReciver_GetStatus())
      {
        //Serial.println("RECEIVER READY");
        char received_string[512];
        KeywordCode eTokenKey;

        Receiver_GetStringCopy(received_string);
        //Serial.println("Received string: " + String(received_string));
        DecodeMsg(received_string);
        if(OK == eToken_GetKeywordCode(0,&eTokenKey))
        {
          switch(eTokenKey)
          {
            case HELLO:
            {
            char tekst[] = "Hi!!!\n";
            FILE* file = fopen("/tmp/ble_pipe_in", "w");
            size_t ch_count = fwrite(tekst, sizeof(char), strlen(tekst), file);
            fclose(file);
            //Serial.println("Written: " + String((int)ch_count) + " chars");
            //system("echo \"10\r\n\" > /dev/rfcomm0");
            break;
            }
            case GET_ECG:
            {
            char tekst[32];
            if((digitalRead(10) == 1)||(digitalRead(11) == 1))
            {
              if((digitalRead(10) == 1)&&(digitalRead(11) == 1))
                strncpy(tekst, "GET_ECG ERR LO-&LO+\n\0", sizeof(tekst));
              else if(digitalRead(10) == 1)
                strncpy(tekst, "GET_ECG ERR LO+\n\0", sizeof(tekst));
              else if(digitalRead(11) == 1)
                strncpy(tekst, "GET_ECG ERR LO-\n\0", sizeof(tekst));
            }
            else
            {
              char num[4];
              sprintf(num, "%d", analogRead(A0));
              strncpy(tekst, "GET_ECG OK ", sizeof(tekst));
              strcat(tekst, num);
              strcat(tekst, "\n\0");
            }
            //Serial.println(tekst);
            FILE* file = fopen("/tmp/ble_pipe_in", "w");
            size_t ch_count = fwrite(tekst, sizeof(char), strlen(tekst), file);
            fclose(file);
            break;
            }
            case SAVE_ON_SD:
            {
              if(OK == eToken_GetKeywordCode(1,&eTokenKey)) {
                if(eTokenKey == ON) {
                  if(OK == eToken_GetString(2,file_name)) {
                    if(OK == eToken_GetString(3,user_name)) {
                      save_on_sd = true;
                      pthread_t newThread;
                      int p = pthread_create(&newThread, NULL, save_data, (void*)0);
                      if(p)
                        Serial.println("Error: Unable to create thread");
                      else
                        pthread_detach(newThread);
                    }
                  }
                }else {
                  save_on_sd = false;
                }
                char tekst[] = "SAVE_ON_SD OK\n";
                FILE* file = fopen("/tmp/ble_pipe_in", "w");
                size_t ch_count = fwrite(tekst, sizeof(char), strlen(tekst), file);
                fclose(file);
              }else {
                char tekst[] = "SAVE_ON_SD ERR\n";
                FILE* file = fopen("/tmp/ble_pipe_in", "w");
                size_t ch_count = fwrite(tekst, sizeof(char), strlen(tekst), file);
                fclose(file);
              }
              break;
            }
            case UNMOUNT_SD:
            {
              system("umount /media/sdcard");
              Serial.println("SD CARD UNMOUNTED");
              char tekst[] = "UNMOUNT_SD OK\n";
              FILE* file = fopen("/tmp/ble_pipe_in", "w");
              size_t ch_count = fwrite(tekst, sizeof(char), strlen(tekst), file);
              fclose(file);
              break;
            }
            case GET_FILE:
            {
              if(OK == eToken_GetString(1,user_id)) {
                if(isEnd)
                  root = SD.open("/");
                if(getNextFile(root, 0))
                  isEnd = false;
                else
                  isEnd = true;
              
              }
              break;
            }
            case GET_VALUES:
            {
              Serial.println("GET_VALUES");
              if(OK == eToken_GetString(1,file_name)) {
                if(isEnd) {
                  Serial.println("END");
                  root = SD.open(file_name, FILE_READ);
                  while(root.available()) {
                    char c = (char)root.read();
                    if(c == '\n') break;
                  }
                }
                Serial.println("FUNCTION");
                if(get_values(root))
                  isEnd = false;
                else
                  isEnd = true;
              }
              break;
            }
            case UPLOAD:
            {
              if(OK == eToken_GetString(1,plik)) {
                upload_status = 0;
                save_on_sd = false;
                /*
                pthread_t newThread;
                int p = pthread_create(&newThread, NULL, upload_file, (void*)0);
                if(p)
                  Serial.println("Error: Unable to create thread");
                else
                  pthread_detach(newThread);
                  */
              }
              char tekst[] = "UPLOAD OK\n";
              FILE* file = fopen("/tmp/ble_pipe_in", "w");
              size_t ch_count = fwrite(tekst, sizeof(char), strlen(tekst), file);
              fclose(file);
              break;
            }
            case GET_UPLOAD_STATUS:
            {
              char tekst[32];
              char num[4];
              sprintf(num, "%d", upload_status);
              strncpy(tekst, "GET_UPLOAD_STATUS OK ", sizeof(tekst));
              strcat(tekst, num);
              strcat(tekst, "\n\0");
              
              FILE* file = fopen("/tmp/ble_pipe_in", "w");
              size_t ch_count = fwrite(tekst, sizeof(char), strlen(tekst), file);
              fclose(file);
              break;
            }
          }
        }
        else{
          if(redecode_msg < 10)
          {
            redecode_msg++;
            goto REDECODE_MSG;
          }
          redecode_msg = 0;
          Serial.println("WRONG TOKEN");
          char tekst[] = "WRONG_TOKEN\n";
          FILE* file = fopen("/tmp/ble_pipe_in", "w");
          size_t ch_count = fwrite(tekst, sizeof(char), strlen(tekst), file);
          fclose(file);
        }
        memset(received_buffer, 0 , 1024);
      }
    }
  fclose(f);
  }
}

bool get_values(File dir) {
  Serial.println("START");
  //char values[500];
  String values = "";
  String val;
  bool isEnd = false;
  if(!dir.available()) {
    dir.close();
      char tekst[] = "GET_VALUES ERR NO_MORE_VALUES\n";
      FILE* file = fopen("/tmp/ble_pipe_in", "w");
      size_t ch_count = fwrite(tekst, sizeof(char), strlen(tekst), file);
      fclose(file);
    return false;
  }

    for(int i = 0; i < 100; i++) {
      int temp;
      val = "";
      while(true) {
        if(!dir.available())
        {
          isEnd = true;
          break;
        }
        temp = int(dir.read());
        if(!isdigit(temp)) {
          if(char(temp) == 'E') {
            val += "0";
            while(true) {
              char c = (char)dir.read();
              if(c == '\r') break;
            }
            break;
          }
          else if(char(temp) == ' ') {
            val += '_';
            continue;
          }
          else
            break;
        }
        val += char(temp);
      }
      Serial.println(val);
      values += val + ",";
      if(isEnd)
      {
        break;
      }
      //val[3] = '\0';
      /*
      if(!dir.available())
      {
        isEnd = true;
        break;
      }
      dir.read();*/
      if(!dir.available())
      {
        isEnd = true;
        break;
      }
      dir.read();
      //char num[4];
      //sprintf(num, "%d", myFile.read());
      //Serial.println(values);
      //values += val;
      //AppendString(val, values);
      //strcat(values, val);
      //Serial.println(values);
      if(!dir.available())
      {
        isEnd = true;
        break;
      }
      //if(i != 98)
        //values += ",";
        //AppendString(",", values);
      Serial.println(i);
      //Serial.println(values);
    }
  Serial.println(values);
      char tekst[4000];
      strncpy(tekst, "GET_VALUES OK ", sizeof(tekst));
      char __values[values.length()];
      values.toCharArray(__values, sizeof(__values));
      strcat(tekst, __values);
      strcat(tekst, "\n\0");
      FILE* file = fopen("/tmp/ble_pipe_in", "w");
      size_t ch_count = fwrite(tekst, sizeof(char), strlen(tekst), file);
      fclose(file);
  //dir.close();
  return true;
}

bool getNextFile(File dir, int numTabs) {
  while (true) {

    File entry =  dir.openNextFile();
    if (!entry) {
      // no more files
      char tekst[] = "GET_FILE ERR NO_MORE_FILES\n";
      FILE* file = fopen("/tmp/ble_pipe_in", "w");
      size_t ch_count = fwrite(tekst, sizeof(char), strlen(tekst), file);
      fclose(file);
      return false;
    }
    for (uint8_t i = 0; i < numTabs; i++) {
      Serial.print('\t');
    }
    //Serial.print(entry.name());
    if (entry.isDirectory()) {
      entry.close();
      continue;
    } else {
      int i;
      char id[128];
      for(i = 0; isdigit(entry.name()[i]) || entry.name()[i] == NULL; i++)
      {
        id[i] = entry.name()[i];
      }
      id[i] = '\0';
      if(EQUAL != eCompareString(id, user_id)){
        entry.close();
        continue;
      }
      char tekst[64];
      strncpy(tekst, "GET_FILE OK ", sizeof(tekst));
      strcat(tekst, entry.name());
      strcat(tekst, "\n\0");
      FILE* file = fopen("/tmp/ble_pipe_in", "w");
      size_t ch_count = fwrite(tekst, sizeof(char), strlen(tekst), file);
      fclose(file);
    }
    entry.close();
    break;
  }
  return true;
}

/*
void printDirectory(File dir, int numTabs) {
  while (true) {

    File entry =  dir.openNextFile();
    if (! entry) {
      // no more files
      break;
    }
    for (uint8_t i = 0; i < numTabs; i++) {
      Serial.print('\t');
    }
    Serial.print(entry.name());
    if (entry.isDirectory()) {
      Serial.println("/");
      printDirectory(entry, numTabs + 1);
    } else {
      // files have sizes, directories do not
      Serial.print("\t\t");
      Serial.println(entry.size(), DEC);
    }
    entry.close();
  }
}

/*
void nextFile()
{
  static File dir;
  static bool isEnd = true;
  if(isEnd) {
    Serial.println("Opening dir");
    File dir = SD.open("/", FILE_READ);
    if(!dir) {
      char tekst1[] = "GET_FILE ERR NO_FILES\n";
      FILE* file = fopen("/tmp/ble_pipe_in", "w");
      size_t ch_count = fwrite(tekst1, sizeof(char), strlen(tekst1), file);
      fclose(file);
      dir.close();
      return;
    }
    isEnd = false;
  }
  Serial.println("Opening file");
  File entry = dir.openNextFile();
  Serial.println("File opened");
  if(!entry) {
    char tekst1[] = "GET_FILE ERR NO_MORE_FILES\n";
    FILE* file = fopen("/tmp/ble_pipe_in", "w");
    size_t ch_count = fwrite(tekst1, sizeof(char), strlen(tekst1), file);
    fclose(file);
    isEnd = true;
    entry.close();
    dir.close();
    return;
  }
  char tekst[32];
  strncpy(tekst, "GET_FILE OK ", sizeof(tekst));
  strcat(tekst, entry.name());
  strcat(tekst, "\n\0");
  FILE* file = fopen("/tmp/ble_pipe_in", "w");
  size_t ch_count = fwrite(tekst, sizeof(char), strlen(tekst), file);
  fclose(file);
  entry.close();
}
*/

void *save_data(void* x) {
  Serial.print("Creating file ");
  Serial.println(file_name);
  Serial.println(user_name);
  File file = SD.open(file_name, FILE_WRITE);
  bool firstIteration = true;
  long start;
  long check_time;
  if(file) {
    file.println(user_name);
    while(save_on_sd) {
      //if(!firstIteration) save_time = micros(); else save_time = 0;
      //save_time = micros();
      if((digitalRead(10) == 1)||(digitalRead(11) == 1))
      {
        if((digitalRead(10) == 1)&&(digitalRead(11) == 1)) {
          //file.println("ERR LO-&LO+");
          if(firstIteration) {file.print(0); firstIteration = false; } else file.print(micros()-start);
          file.print(" ");
          file.println("ERR_LO-&LO+");
        }
        else if(digitalRead(10) == 1) {
          //file.println("ERR LO+");
          if(firstIteration) {file.print(0); firstIteration = false; } else file.print(micros()-start);
          file.print(" ");
          file.println("ERR_LO+");
        }
        else if(digitalRead(11) == 1) {
          //file.println("ERR LO-");
          if(firstIteration) {file.print(0); firstIteration = false; } else file.print(micros()-start);
          file.print(" ");
          file.println("ERR_LO-");
        }
      }
      else {
        if(firstIteration) {file.print(0); firstIteration = false; } else file.print(micros()-start);
        file.print(" ");
        file.println(analogRead(A0));
      }
      start = micros();
      while(micros() - start < 5000) {}
    }
  }
  else
    Serial.println("Error creating file...");
  Serial.println("Closing file...");
  file.close();
}
/*
void *upload_file(void *x) {
  //char plik[] = "2017-11-02_16-20-58.csv";
  upload_status = 0;
  File myFile = SD.open(plik, FILE_READ);
  if(!(plik[4] == '-' && plik[7] == '-' && plik[10] == '_' && plik[13] == '-' && plik[16] == '-' && plik[19] == '.' && plik[20] == 'c' && plik[21] == 's' && plik[22] == 'v'))
    return x;
  plik[19] = '\0';
  char datetime[30];
  strncpy(datetime, "date=", sizeof(datetime));
  strcat(datetime, plik);
  datetime[15] = ' ';
  datetime[18] = ':';
  datetime[21] = ':';
  char stime[10];
  for(int i = 0; datetime[i] != NULL; i++)
    stime[i] = datetime[i+16];
  datetime[15] = '%';
  datetime[16] = '2';
  datetime[17] = '0';
  datetime[18] = '\0';
  strcat(datetime, stime);
  Serial.println(datetime);
  
  send_data(client, "/~milchalt/myECG/add_signal.php?", datetime);

  //int values[] = {1,2,3,4,5};
  char data[1500];
  char val[3];
  bool isEnd = false;
  if(!myFile.available()) {
    myFile.close();
    return x;
  }
  while(true) {
    memset(data, 0, sizeof(data));
    for(int i = 0; i < 100; i++) {
      strcat(data, "value[]=");
      memset(val, 0 , 3);
      for(int i = 0; i < 3; i++) {
        if(!myFile.available())
        {
          isEnd = true;
          break;
        }
        val[i] = char(myFile.read());
      }
      if(isEnd)
        break;
      val[3] = '\0';
      if(!myFile.available())
      {
        isEnd = true;
        break;
      }
      myFile.read();
      if(!myFile.available())
      {
        isEnd = true;
        break;
      }
      myFile.read();
      //char num[4];
      //sprintf(num, "%d", myFile.read());
      strcat(data, val);
      if(!myFile.available())
      {
        isEnd = true;
        break;
      }
      if(i != 99)
        strcat(data, "&");
    }
    Serial.println(data);
    send_data(client, "/~milchalt/myECG/add.php?", data);
    if(isEnd)
      break;
  }
  upload_status = 100;
  //Serial.println("HERE");
  if(myFile)
    myFile.close();
}
*/

/*
void send_data(EthernetClient& client_database, char* address, char* data)
{
  //char datac[16];
  //memset(datac,0,16);
  //sprintf(datac, "%.2f", bme.getTemperature_C());
  //data = "temp=" + String(datac);

  bool udalo_sie = true;
  if(client_database.connect("student.agh.edu.pl", 80))
  {
    //Serial.println("POST " + String(address) + String(data) +" HTTP/1.1");
    client_database.println("POST " + String(address) + String(data) +" HTTP/1.1"); 
    //client_database.println("POST /~milchalt/test/add.php?"+data+" HTTP/1.1"); 
    //client_database.println("GET /~milchalt/test/add.php? HTTP/1.1"); 
    client_database.println("Host: student.agh.edu.pl"); // SERVER ADDRESS HERE TOO

    client_database.println("Content-Type: application/x-www-form-urlencoded"); 
    client_database.print("Content-Length: "); 
    client_database.println(String(data).length()); 
    client_database.println(); 
    client_database.print(data);
    
 }
 else
  udalo_sie = false;

 if (client_database.connected()) { 
 client_database.stop(); // DISCONNECT FROM THE SERVER
 }
 Serial.println("DONE " + String(udalo_sie));
 
}

*/

